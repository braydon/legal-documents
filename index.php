<?php

require_once('odtphp-1.0.1/library/odf.php');

$config = array(
    'ZIP_PROXY' => 'PhpZipProxy',
    'DELIMITER_LEFT' => '{{',
    'DELIMITER_RIGHT' => '}}'
);

$odf = new odf("software-disclosure-agreement.odt", $config);

if ( $_POST ) {

	$odf->setVars('date', $_POST['date'] );
	$odf->setVars('discloser', $_POST['discloser'] );
	$odf->setVars('recipient', $_POST['recipient'] );
	$odf->setVars('state', $_POST['state'] );

	$path = dirname(__FILE__);

	$odftmp = tempnam(sys_get_temp_dir(), 'software-disclosure-agreement-');

	$odf->exportAsAttachedFile();

} else {

?>

<form method="post">
	<p>
		<label for="discloser">Discloser</label>
		<input type="text" name="discloser"></input>
	</p>

	<p>		
		<label for="recipient">Recipient</label>
		<input type="text" name="recipient"></input>
	</p>

	<p>
		<label for="date">Date</label>
		<input type="text" name="date"></input>
	</p>

	<p>
		<label for="state">State</label>
		<input type="text" name="state"></input>
	</p>

	<input type="submit" value="Download"/>

</form>

<?php
}

 
?>
